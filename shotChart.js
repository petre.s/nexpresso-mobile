import * as React from 'react';
import { View, Button, FlatList, StyleSheet, Text, SafeAreaView, SectionList, ScrollView, StatusBar } from 'react-native';
import { Chart, HorizontalAxis, Line, VerticalAxis } from 'react-native-responsive-linechart';

import { Component } from 'react';
import BluetoothService from './bluetoothService';
import AsyncStorage from '@react-native-async-storage/async-storage';



export default class ShotChart extends Component {
  constructor() {
    super()
    this.count = 0;
    this.flowCount = 0;
    this.dummyValue = true;
    this.dummyValueFlow = true;

    this.state = {
      pressureValues: [{ "x": 0, "y": 0 }],
      flowValues: [{ "x": 0, "y": 0 }],
      maxY: 100,
      startX: 0,
      endX: 1,
      middlePoint: 0,
      graphHistory: [],
      shotTimer: 0
    }
    this.storagePromise = this.populateGraphHistory()

    // console.log("passing")

    BluetoothService.subscribeToShotParams(((o) => {
      this.addDataPoint(o.time, o.pressure, o.flow);
    }));

  }

  // addValue(v){
  //   if (this.dummyValue){
  //     this.dummyValue = false;
  //     this.state.pressureValues.splice(0,1);
  //   }

  //   this.state.pressureValues.push({ x: this.count++, y: v })

  //     this.setState({
  //       endX:  this.count
  //     })
  // }

  // addValueFlow(v){
  //   if (this.dummyValueFlow){
  //     this.dummyValueFlow = false;
  //     this.state.flowValues.splice(0,1);
  //   }

  //   this.state.flowValues.push({ x: this.flowCount++, y: v })

  //     this.setState({
  //       endX:  this.count
  //       //  startX : this.state.pressureValues[0].x
  //     })
  // }

  createNewShotInPersistance() {
    return AsyncStorage.getItem('@shot_history2').then(shotHistoryJson => {
      var shotHistory
      if (!shotHistoryJson) {
        console.log('shotHistory nothing found in  storage')
        shotHistory = {
          shots: []
        }
      } else {
        shotHistory = JSON.parse(shotHistoryJson)
        // console.log(['shotHistory', shotHistory])
      }

      shotHistory.shots.push({ time: new Date(), pressure: [], flow: [] })

      return AsyncStorage.setItem('@shot_history2', JSON.stringify(shotHistory));

    })

  }

  persistDataPoint(time, pressure, flow) {
    return AsyncStorage.getItem('@shot_history2').then(shotHistoryJson => {
      var shotHistory
      if (!shotHistoryJson) {
        return;
      } else {
        shotHistory = JSON.parse(shotHistoryJson)
      }

      var lastShot = shotHistory.shots[shotHistory.shots.length - 1]

      lastShot.pressure.push({ time: time, pressure: pressure })
      lastShot.flow.push({ time: time, flow: flow })

      // console.log(['shot points count', lastShot.pressure.length])
      // console.log(['shot points', lastShot.pressure])

      return AsyncStorage.setItem('@shot_history2', JSON.stringify(shotHistory));

    })
  }

  addDataPoint(time, pressure, flow, isReload = false) {
    // console.log(["adding points", time, pressure, flow])
    // return;

    if (time == 0) {

      if (!isReload) {
        console.log("storagePromise")
        this.storagePromise = this.storagePromise.then(() => this.createNewShotInPersistance()).then(() => this.populateGraphHistory())
      }

      // this.dummyValue = false;
      console.log("cleaning shot graph")
      this.state.pressureValues.length = 1
      this.state.flowValues.length = 1

      this.state.pressureValues[0].y = pressure
      this.state.flowValues[0].y = flow

      // this.state.pressureValues.splice(0, 1);
      // this.state.flowValues.splice(0, 1);

      // this.setState({ pressureValues: [{ "x": time, "y": pressure }] })
      // this.setState({ flowValues: [{ "x": time, "y": flow }] })
      // flowValues: [{ "x": -1, "y": 0 }],})
    }

    this.setState({ shotTimer: time })
    this.state.pressureValues.push({ x: time, y: pressure })
    this.state.flowValues.push({ x: time, y: flow })

    this.storagePromise = this.storagePromise.then(() => this.persistDataPoint(time, pressure, flow))


    if (this.state.endX < time) {
      this.setState({ endX: time })
    }
  }

  populateGraphHistory() {
    return AsyncStorage.getItem('@shot_history2').then(shotHistoryJson => {
      var shotHistory
      if (!shotHistoryJson) {
        return;
      } else {
        shotHistory = JSON.parse(shotHistoryJson)
      }

      this.setState({
        graphHistory: [{
          title: '',
          data: shotHistory.shots.map((x, i) => { return { text: `Shot ${i}`, index: i } }).reverse()
        }]
      });

    })
  }

  loadGraphForIndex(index) {
    return AsyncStorage.getItem('@shot_history2').then(shotHistoryJson => {
      var shotHistory
      if (!shotHistoryJson) {
        return;
      } else {
        shotHistory = JSON.parse(shotHistoryJson)
      }
      var shot = shotHistory.shots[index]

      for (var i = 0; i < shot.pressure.length; i++) {
        this.addDataPoint(shot.pressure[i].time, shot.pressure[i].pressure, shot.flow[i].flow, true)
      }

    })

  }

  render() {

    const styles = StyleSheet.create({
      shotHistoryContainer: {
        width: 200,
        margin: 20,
        height: 400
        // flex: 1,
        // paddingTop: 500,
      },
      shotHistoryItem: {
        // height: 50,
        // backgroundColor: '#e6e3e3',
        margin: 10
      },
      shotTimer: {
        fontSize: 50,
        paddingLeft: 50
      }
    });


    // const DATA = [
    //   {
    //     title: 'Main dishes',
    //     data: [{ text: 'Pizza', index: 0 }, { text: 'Pizza1', index: 1 }, { text: 'Pizza2', index: 2 }],
    //   },

    // ];
    return (
      <View>
        <Text style={styles.shotTimer}>{this.state.shotTimer}</Text>
        <Chart
          style={{ height: 200, width: '100%', marginTop: 40 }}

          padding={{ left: 40, bottom: 20, right: 20, top: 20 }}
          xDomain={{ min: this.state.startX, max: this.state.endX }}
          yDomain={{ min: 0, max: 10 }}
        >
          <VerticalAxis
            tickValues={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
            theme={{
              axis: { stroke: { color: '#aaa', width: 2 } },
              ticks: { stroke: { color: '#aaa', width: 2 } },
              labels: { formatter: (v: number) => v.toFixed(2) },
            }}
          />
          <HorizontalAxis
            tickCount={Math.ceil(this.state.pressureValues.length / 50 + 1)}
            theme={{
              axis: { stroke: { color: '#aaa', width: 2 } },
              ticks: { stroke: { color: '#aaa', width: 2 } },
              labels: { label: { rotation: 50 }, formatter: (v) => v.toFixed(1), visible: false },
            }}
          />

          <Line smoothing="bezier" tension={0.15} theme={{ stroke: { color: 'blue', width: 2 } }} data={this.state.pressureValues} />
          <Line smoothing="bezier" tension={0.15} theme={{ stroke: { color: 'red', width: 2 } }} data={this.state.flowValues} />

        </Chart>



        <Button
          title="test data"
          onPress={() => {
            // this.addDataPoint(0, -1.96, 0)
            for (var i = 0; i < 10000; i += 200) {
              this.addDataPoint(i / 1000, 9 - (i / 10000) + Math.random(), i / 10000 + Math.random())
            }
          }}
        />

        <SafeAreaView style={styles.shotHistoryContainer}>
          <ScrollView>
            <SectionList
              sections={this.state.graphHistory}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item }) => (
                <View style={styles.shotHistoryItem}>
                  <Button title={item.text} onPress={() => this.loadGraphForIndex(item.index)}></Button>
                </View>
              )}
            // renderSectionHeader={({ section: { title } }) => ( // group by date here...
            //   <Text>{title}</Text>
            // )}
            />
          </ScrollView>
        </SafeAreaView>



      </View>
    );
  }
}