import * as React from 'react';
import { StyleSheet, useWindowDimensions, View } from 'react-native';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import SettingsScreen from './settingsScreen';
import TemperatureScreen from './temperatureScreen';
import BrewLimitStep from './brewLimitStep'
import StrategyView from './strategyView'
import StrategyRaw from './strategyRaw';

import ShotChart from './shotChart';
import Icon from 'react-native-vector-icons/FontAwesome';

const FirstRoute = () => (
  <View style={{
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 50
  }} >

    <TemperatureScreen></TemperatureScreen>
  </View>
);


const SecondRoute = () => (
  <View style={{ flex: 1 }} >
    <SettingsScreen></SettingsScreen>
  </View>
);

const ThirdRoute = () => (
  <View style={{ flex: 1 }} >

    {/* <BrewLimitStep brewPhase={0}></BrewLimitStep> */}
  </View>
);
const ForthRoute = () => (
  <View style={{ flex: 1 }} >
    {/* <BrewLimitStep brewPhase={1}></BrewLimitStep> */}
  </View>
);

const StratRoute = () => (

  <StrategyRaw></StrategyRaw>

);

const ShotRoute = () => (
  <ShotChart />
);

const renderScene = SceneMap({
  shot: ShotRoute,
  first: FirstRoute,
  second: SecondRoute,
  strat: StratRoute,

});

export default function TabViewExample() {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    // { key: 'third', title: 'Phase 1', icon: 'coffee' },
    // { key: 'forth', title: 'Phase 2', icon: 'coffee' },
    { key: 'shot', title: 'Shot', icon: 'coffee' },
    { key: 'strat', title: 'Strategy', icon: 'coffee' },
    { key: 'first', title: 'Temperature', icon: 'thermometer-half' },
    { key: 'second', title: 'Settings', icon: 'gear' },

  ]);

  const styles = StyleSheet.create({
    tabBar: {
      backgroundColor: '#f7f7f7',
      borderTopColor: "#c8c8c8",
      borderBottomWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      borderTopWidth: 0.5,
      height: 58,
    },
    labelStyle: {
      fontSize: 10,
      fontWeight: "600",
      textTransform: 'none'
    }
  })

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: '#f7f7f7' }}

      style={styles.tabBar}
      getLabelText={({ route }) => route.title}
      labelStyle={styles.labelStyle}
      activeColor="#1797ff"
      inactiveColor='#9d9d9e'
      tabStyle={{ backgroundColor: '#f7f7f7', marginTop: -5 }}
      gap={1}
      renderIcon={({ route, focused, color }) => (
        <Icon
          name={route.icon}
          color={color}
          size={27}
          solid
        />
      )}

    />
  );

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={{ width: layout.width }}
      tabBarPosition="bottom"
      renderTabBar={renderTabBar}
      swipeEnabled={false}
      animationEnabled={false}
    />
  );
}