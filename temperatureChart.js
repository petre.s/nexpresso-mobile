import * as React from 'react';
import { View } from 'react-native';
import { Chart, HorizontalAxis, Line, VerticalAxis } from 'react-native-responsive-linechart';

import { Component } from 'react';
import BluetoothService from './bluetoothService';



export default class TemperatureChart extends Component {
  constructor() {
    super()
    this.count = 0;

    this.state = { 
      temperatureValues: [{ "x": 0, "y": 0 }], 
      maxY:100,
      startX : 1,
      endX : 10,
      targetTemp :0
    }

    console.log("passing")
    BluetoothService.subscribeToTemperature((v => {
   
    }));

    setInterval(() => {
      if (BluetoothService){
      BluetoothService.readLastTemp().then(v=>{
        if (v){
          this.addValue(v)
        }
      }).catch(e=>{})
    }
    }, 1000);


    BluetoothService.subscribeToTargetTemperature((val =>{
        if (val != this.state.targetTemp){
          console.log([val, "target"])
          this.setState({targetTemp: val})
        }
    }));

  }


  addValue(v){
    this.state.temperatureValues.push({ x: this.count++, y: v })
    if (v > this.state.maxY){
      this.setState({maxY: v})
    }

    // if (this.count > this.state.endX){
      
    // }

    this.setState(
      { temperatureValues: this.state.temperatureValues.slice(-60) });
    
      this.setState({
        endX:  this.count, 
        startX : this.state.temperatureValues[0].x
      })
    
    // console.log(this.state.temperatureValues.length)
  }

  render() {

    return (
      <View>
        <Chart
          style={{ height: 200, width: '100%', marginTop: 40 }}

          padding={{ left: 40, bottom: 20, right: 20, top: 20 }}
          xDomain={{ min: this.state.startX, max: this.state.endX }}
          yDomain={{ min: this.state.targetTemp-10, max: this.state.targetTemp+10 }}
        >
          <VerticalAxis
            tickValues={[this.state.targetTemp]}
            theme={{
              axis: { stroke: { color: '#aaa', width: 2 } },
              ticks: { stroke: { color: '#aaa', width: 2 } },
              labels: { formatter: (v: number) => v.toFixed(2) },
            }}
          />
          <HorizontalAxis
            tickCount={Math.ceil(this.state.temperatureValues.length/2)}
            theme={{
              axis: { stroke: { color: '#aaa', width: 2 } },
              ticks: { stroke: { color: '#aaa', width: 2 } },
              labels: { label: { rotation: 50 }, formatter: (v) => v.toFixed(1), visible: false },
            }}
          />

          <Line smoothing="bezier" tension={0.15} theme={{ stroke: { color: 'blue', width: 2 } }} data={this.state.temperatureValues} />

        </Chart>
        {/* </Container> */}
      </View>
    );
  }
}