

import React, { Component, useState } from 'react';

import { StyleSheet, Text, View, Button, Modal, Pressable, FlatList, ScrollView } from 'react-native';
import BluetoothService from './bluetoothService';


export default class SettingsScreen extends Component {
    constructor() {
        super()
         console.log("fdsa")

        this.state = {
            connected: false,
            modalVisible: false,
            connectionInProgress: false,
            btDevices: []
        };
        console.log(["set in connectionInProgress", this.state.connectionInProgress])
        BluetoothService.onConnected((val) => {

            this.setState({ connected: true })
            this.setState({ deviceName: val })
        });
        BluetoothService.onDisconnected(() => this.setState({ connected: false }));
    }

    render() {
        const styles = StyleSheet.create({
            centeredView: {
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                marginTop: 22
            },
            modalView: {
                margin: 20,
                backgroundColor: "white",
                borderRadius: 20,
                padding: 35,
                alignItems: "center",
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5
            },
            button: {
                borderRadius: 20,
                padding: 10,
                elevation: 2
            },
            buttonOpen: {
                backgroundColor: "#F194FF",
            },
            buttonClose: {
                backgroundColor: "#2196F3",
            },
            textStyle: {
                color: "white",
                fontWeight: "bold",
                textAlign: "center"
            },
            modalText: {
                marginBottom: 15,
                textAlign: "center"
            }
        });


        const listStyles = StyleSheet.create({
            container: {
                // flex: 1,
                paddingTop: 22,
                // height: 300
            },
            item: {
                padding: 10,
                fontSize: 18,
                height: 44,
            },
        });

        return (
            <View style={{
                height: '100%',
                // flexDirection:'column-reverse'
                justifyContent: 'center'
            }}>

                <Text>Settings</Text>

                {(BluetoothService.isConnected() || this.state.connected) && 
                    <DeviceConnectedView></DeviceConnectedView>}

                <View style={styles.centeredView}>
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setState({ modalVisible: false })
                        }}
                    >
                        <View style={styles.centeredView}>
                            <View style={styles.modalView}>

                                <View style={{ height: 300, width: 200 }}>
                                    <ScrollView>
                                        <View onStartShouldSetResponder={() => true}>
                                            <View style={listStyles.container}>
                                                <FlatList
                                                    data={this.state.btDevices}

                                                    renderItem={({ item }) =>

                                                        <View>
                                                            <Pressable onPress={() => {
                                                                this.setState({ modalVisible: false, connectionInProgress:true })
                                                                
                                                                
                                                                BluetoothService.connectToDevice(item).then(v=> this.setState({  deviceName : v, connected:true }))
                                                                .finally(() =>this.setState({  connectionInProgress:false }));
                                                            }}>

                                                                <Text style={listStyles.item}>{item.name}</Text>
                                                            </Pressable>
                                                        </View>

                                                    }
                                                />
                                            </View>
                                        </View>
                                    </ScrollView>
                                </View>

                                <Button
                                    title="Close"
                                    onPress={() => {
                                        this.setState({ modalVisible: false })
                                        BluetoothService.stopDeviceScan()
                                    }}
                                />

                            </View>
                        </View>
                    </Modal>
                    {!this.state.connectionInProgress &&
                    <Button
                        title="Search for BT devices"
                        onPress={() => {
                            this.setState({ modalVisible: true })
                            this.searchForBTDevices()
                        }}
                    />}

                </View>


            </View>);
    }
    searchForBTDevices() {
        this.setState({ btDevices: [] })
        BluetoothService.startDeviceScan((device => {
            if (device && device.name
                && (this.state.btDevices.filter(d => d.name == device.name).length == 0)
            ) {
                this.setState({ btDevices: [...this.state.btDevices, device] })
            }
        }))
    }

}


class DeviceConnectedView extends Component {
    constructor() {
        super()
        this.state = { deviceName: null };

        BluetoothService.onConnected((val) => {
            // console.log("helllllloooo")
            this.setState({ deviceName: val, connectionInProgress:false })
            console.log(["set in connectionInProgress", this.state.connectionInProgress])
        });

    }

    render() {
        return (
            <View style={{
                height: '100%',
                // flexDirection:'column-reverse'
                justifyContent: 'center'
            }}>

                <Text style={{
                    fontWeight: "bold",
                    marginVertical: 4,
                    fontSize: 40,
                    color: '#1797ff'
                }}
                >Connected
                </Text>
                
                <Text style={{
                    fontWeight: "bold",
                    marginVertical: 4,
                    fontSize: 40,
                    color: '#1797ff'
                }}
                >{
                        this.state.deviceName == null ? BluetoothService.getConnectedDeviceName() : this.state.deviceName}
                </Text>

                <Button
                    title="Remove this connection"
                    onPress={() => {
                        BluetoothService.disconnect() 
                    
                    }
                 }
                />

            </View>);
    }
}