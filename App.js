
import { StyleSheet, View } from 'react-native';
import {
  GestureHandlerRootView
} from 'react-native-gesture-handler';
import {request, PERMISSIONS} from 'react-native-permissions';
import { LogBox } from 'react-native';

import TabViewExample from './tabView';


export default function App() {

  request(PERMISSIONS.ANDROID.BLUETOOTH_SCAN).then((result) => {
      console.log(["BLUETOOTH_SCAN", result]);
  });

  request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {
    console.log(["ACCESS_FINE_LOCATION", result]);
});


request(PERMISSIONS.ANDROID.BLUETOOTH_ADVERTISE).then((result) => {
  console.log(["BLUETOOTH_ADVERTISE", result]);
});



  request(PERMISSIONS.ANDROID.BLUETOOTH_CONNECT).then((result) => {
    console.log(["BLUETOOTH_CONNECT", result]);
  });

  LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message
  LogBox.ignoreAllLogs(); //Ignore all log notifications

  return (

    <GestureHandlerRootView style={{ flex: 1 }}>
    <View style={styles.container}>
     

    <TabViewExample/>
    
    </View>
    </GestureHandlerRootView>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 0,
    marginRight: 0,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
});
