

import React, { Component } from 'react';

import { View } from 'react-native';
import TemperatureChart from './temperatureChart';

import TemperatureSlider from './temperatureSlider';

export default class TemperatureScreen extends Component {
    render() {
        return (
            <View style={{
                height: '100%',
                // flexDirection:'column-reverse'
                justifyContent: 'center'
            }}>

    <TemperatureChart/>
    <TemperatureSlider />

            </View>);
        }
}