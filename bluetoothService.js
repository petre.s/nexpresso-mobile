import { BleManager, LogLevel } from 'react-native-ble-plx';
import { Buffer } from "buffer";
import { Observable, Subject } from 'rxjs';
import AsyncStorage from '@react-native-async-storage/async-storage';


const temp_reading_ch_uuid = "beaffbdc-ad3b-4472-90d0-738397730fd1"
const temp_target_ch_uuid = "9a1af612-c621-46d0-9e26-08d7905c4813"
const temp_set_ch_uuid = "080d9e6b-0b2a-432c-a6ce-d740b66c2e1d"


const shot_params_characteristic_uuid = "323d5bdc-4df0-4fbc-beba-81c65d484ddd"


const main_service_uuid = "17754bf9-fe23-4907-a27f-b0424e1c6713"

const strategy_characteristic_uuid = "cc384ad7-2b35-43f5-bc4b-d16e2fa0c55d"

manager = new BleManager();
manager.setLogLevel(LogLevel.Verbose)
var mainDevice;
var temperatureObs = new Subject()

var strategyObs = new Subject()

var onConnectedHandler = new Subject()
var onDisconnectedHandler = new Subject()

var targetTemperatureObs = new Subject()

var shotParamsObs = new Subject()

function init() {
    console.log("ble initing newadada")


    const subscription = manager.onStateChange((state) => {
        if (state === 'PoweredOn') {
            scanAndConnect();
            subscription.remove();
        }
    }, true);
    scanAndConnect();
}
var setTemperatureCh;
var strategyCh;
var targetTempCh;
var shotParamsCh;
var tempMeasurementCh;

init()

function scanAndConnect() {
    AsyncStorage.getItem('@bt_device_name').then(dev_name => {

        if (!dev_name) {
            console.log('nothing found in  storage')
            return;
        }

        console.log("scanAndConnect")
        manager.startDeviceScan(null, null, (error, device) => {

            if (error) {
                if (error.message != "BluetoothLE is in unknown state") {
                    console.log(["startDeviceScanError", error])
                }

                console.log(["scanAndConnect", error]);
                // Handle error (scanning will be stopped automatically)
                return
            }
            console.log(device.name)

            // Check if it is a device you are looking for based on advertisement data
            // or other criteria.
            if (device.name === dev_name) {
                BluetoothService.connectToDevice(device)
            }
        });
    })
}


const BluetoothService = {

    subscribeToTemperature: function (value) {
        return temperatureObs.subscribe(value)
    },

    uploadStrategy: function (value) {

        toSend = Buffer.from(JSON.stringify(value)).toString('base64')
        console.log(["uploadStrategy..", value, toSend])
        return strategyCh.writeWithResponse(toSend).then(
            () => console.log("done sending")).catch(e => console.log(e));
    },

    uploadStrategyRaw: function (strategyStr) {

        toSend = Buffer.from(strategyStr).toString('base64')
        console.log(["uploadStrategy..", toSend])
        return strategyCh.writeWithResponse(toSend).then(
            () => console.log("done sending")).catch(e => console.log(e));
    },

    subscribeToStrategy: function (value) {
        return strategyObs.subscribe(value)
    },

    subscribeToTargetTemperature: function (value) {
        return targetTemperatureObs.subscribe(value)
    },

    subscribeToShotParams: function (value) {
        return shotParamsObs.subscribe(value)
    },

    setTemperature: function (temp) {
        if (setTemperatureCh) {

            tempInt = Math.floor(temp * 100)
            console.log(Buffer.from([1, 2]));
            toSend = Buffer.from(longToByteArray(tempInt)).toString('base64')
            console.log(["Setting temp..", temp, setTemperatureCh, toSend])
            return setTemperatureCh.writeWithResponse(toSend).then(() => console.log("done sending")).catch(e => console.log(e));
        } else {
            return Promise.reject();
        }
    },

    readLastTemp: function () {
        if (!tempMeasurementCh) {
            return Promise.reject(new Error("not connected yet"));
        }

        return tempMeasurementCh.read().then(c => {

            const readValueInBase64 = c.value;

            const readValueInRawBytes = Buffer.from(readValueInBase64, 'base64');

            const heightMostSignificantByte = readValueInRawBytes[1];
            const heightLeastSignificantByte = readValueInRawBytes[0];

            const heightInCentimeters = (heightMostSignificantByte << 8) | heightLeastSignificantByte;
            
            console.log(Math.round(heightInCentimeters / 100 * 2) / 2)
            
            return Math.round(heightInCentimeters / 100 * 2) / 2
        }).catch(e => {
            console.log(e);
        });
    },

    readLastStrategy: function () {
        console.log(["readLastStrategy"])
        if (!strategyCh) {
            console.log(["Promise.reject(new Error(not connected yet));"])
            return Promise.reject(new Error("not connected yet"));
        }
        // console.log(["readLastStrategy2"])
        return strategyCh.read().then(c => {
            // console.log(["readLastStrategy", c.value])
            const readValueInBase64 = c.value;
            return Buffer.from(readValueInBase64, 'base64').toString("ascii")

        }).catch(e => {
            console.log(e);
        });
    },

    disconnect: function () {
        if (mainDevice) {
            mainDevice.cancelConnection();
        }
        onDisconnectedHandler.next()
        mainDevice = null;
        AsyncStorage.removeItem("@bt_device_name")
    },

    isConnected: function () {
        // console.log(mainDevice);
        return mainDevice != null;
    },

    getConnectedDeviceName: function () {
        if (mainDevice) {
            return mainDevice.name;
        } else {
            return null
        }
    },

    onConnected: function (handler) {
        return onConnectedHandler.subscribe(handler);
    },

    onDisconnected: function (handler) {
        return onDisconnectedHandler.subscribe(handler);
    },

    startDeviceScan: function (handler) {
        manager.startDeviceScan(null, null, (error, device) => { if (!error) handler(device) })
    },
    stopDeviceScan: function () {
        manager.stopDeviceScan()
    },

    connectToDevice: function (device) {
        manager.stopDeviceScan();

        return device.connect()
            .then((device) => {
                device.discoverAllServicesAndCharacteristics()
                    .then(dev => {
                        device.services().then(serv => {
                            main_service = serv.filter(s => s.uuid == main_service_uuid)[0]

                            main_service.characteristics().then(ch => {
                                console.log(["all_char", ch])
                                tempMeasurementCh = ch.filter(c => c.uuid == temp_reading_ch_uuid)[0]
                                shotParamsCh = ch.filter(c => c.uuid == shot_params_characteristic_uuid)[0]
                                setTemperatureCh = ch.filter(c => c.uuid == temp_set_ch_uuid)[0]
                                targetTempCh = ch.filter(c => c.uuid == temp_target_ch_uuid)[0]
                                strategyCh = ch.filter(c => c.uuid == strategy_characteristic_uuid)[0]

                                console.log(["targetTempCh", targetTempCh])
                                targetTempCh.monitor(
                                    (e, c) => {
                                        if (e) {
                                            console.log(["read error", e])
                                            return
                                        }

                                        const readValueInBase64 = c.value;

                                        const readValueInRawBytes = Buffer.from(readValueInBase64, 'base64');

                                        const heightMostSignificantByte = readValueInRawBytes[1];
                                        const heightLeastSignificantByte = readValueInRawBytes[0];

                                        const heightInCentimeters = (heightMostSignificantByte << 8) | heightLeastSignificantByte;
                                        // console.log(heightInCentimeters);

                                        //round to the neards 0.5 increment
                                        let deg = Math.round(heightInCentimeters / 100 * 2) / 2

                                        targetTemperatureObs.next(deg)
                                    }

                                );

                                shotParamsCh.monitor(
                                    (e, c) => {

                                        if (e) {
                                            console.log(["read error shotParamsCh", e])
                                            return
                                        }
                                        console.log(["shotParamsCh raw", c.value])
                                        const readValueInBase64 = c.value;
                                        str = Buffer.from(readValueInBase64, 'base64').toString();

                                        console.log(["decoded shotParamsCh", str])
                                        let time = parseInt(str.split(" ")[0]) / 1000.0;

                                        let pressure = parseInt(str.split(" ")[1]) / 100.0;

                                        let flow = parseInt(str.split(" ")[2]) / 100.0;
                                        console.log(["parsed shotParamsCh", time, pressure, flow])
                                        shotParamsObs.next({ time: time, pressure: pressure, flow: flow })
                                    }
                                );



                                // tempMeasurementCh.monitor(
                                //     (e, c) => {
                                //         if (e) {
                                //             console.log(["read error", e])
                                //             return
                                //         }
                                //         const readValueInBase64 = c.value;

                                //         const readValueInRawBytes = Buffer.from(readValueInBase64, 'base64');

                                //         const heightMostSignificantByte = readValueInRawBytes[1];
                                //         const heightLeastSignificantByte = readValueInRawBytes[0];

                                //         const heightInCentimeters = (heightMostSignificantByte << 8) | heightLeastSignificantByte;
                                //         // console.log(heightInCentimeters);

                                //         temperatureObs.next(heightInCentimeters / 100)
                                //     }

                                // );



                                // strategyCh.monitor(
                                //     (e, c) => {
                                //         if (e) {
                                //             console.log(["read error", e])
                                //             return
                                //         }
                                //         console.log(["strategyCh.monitor", c.value])
                                //         const readValueInBase64 = c.value;
                                //         strategyObs.next(JSON.parse(Buffer.from(readValueInBase64, 'base64').toString("ascii")))
                                //     }

                                // );


                            })
                        })
                        mainDevice = dev;
                        console.log(["dev nameeee", dev.name])


                        AsyncStorage.setItem('@bt_device_name', dev.name);

                        onConnectedHandler.next(dev.name)
                    })
                    .catch(e => console.log(["discoverAllServicesAndCharacteristics issue ", e]))
                esp_device = device;
                // console.log(device)
                console.log("connected...");


                // Do work on device with services and characteristics
            })

    }
}

longToByteArray = function (/*long*/long) {
    // we want to represent the input as a 8-bytes array
    var byteArray = [0, 0];

    for (var index = 0; index < byteArray.length; index++) {
        var byte = long & 0xff;
        byteArray[index] = byte;
        long = (long - byte) / 256;
    }

    return byteArray;
}

export default BluetoothService;