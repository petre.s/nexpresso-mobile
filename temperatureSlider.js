
import { Slider } from '@miblanchard/react-native-slider';
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BluetoothService from './bluetoothService';

export default class TemperatureSlider extends Component {
    constructor() {
        super()

        BluetoothService.subscribeToTargetTemperature(val => {
            if (this.state.tempSliderVal == 0) {
                this.setState({ tempSliderVal: val, targetTemp: val })
            }
        })

        this.state = {
            targetTemp: 85,
            tempSliderVal: 0,
        }
    }

    render() {
        return (
            <View
            style={{
                // flexDirection:"column-reverse",
                // alignItems:"flex-end"
            }}
            >

                <Text
                    style={{
                        fontWeight: "bold",
                        marginVertical: 4,
                        fontSize: 40,
                        color: '#1797ff'
                    }}
                >{this.state.targetTemp}°C</Text>


                <Slider
                    value={this.state.tempSliderVal}
                    step={0.5}
                    minimumValue={85}
                    maximumValue={96}
                    onSlidingComplete={value => {

                        this.setState({ targetTemp: parseFloat(value).toFixed(1) })

                        BluetoothService.setTemperature(this.state.targetTemp);
                    }}

                    onValueChange={value => {
                        this.setState({ tempSliderVal: value })
                        this.setState({ targetTemp: parseFloat(value).toFixed(1) })

                    }}
                />
            </View>
        );
    }

}