import * as React from 'react';
import { View, TextInput , StyleSheet, Button, Text} from 'react-native';
import { Chart, HorizontalAxis, Line, VerticalAxis } from 'react-native-responsive-linechart';

import { Component } from 'react';
import BluetoothService from './bluetoothService';



export default class StrategyRaw extends Component {
  
  formatStr1(strVal) {
    // return strVal;
    console.log("formatStr1")
    return JSON.stringify(JSON.parse(strVal), null, 4)
  }

  constructor() {
    super()
    
    this.state = {
      strategyStr:"",
      error: null
    }

    // console.log("passing")

    this.interval = setInterval(() => {
      if (BluetoothService){
      BluetoothService.readLastStrategy().then(v=>{
        if (v){
          
          v = JSON.stringify(JSON.parse(v), null, 2)
          console.log(["var", v])
          this.setState({strategyStr: v })
          console.log(["after state"])
          clearInterval(this.interval)
        }
      }).catch(e=>{})
    }
    }, 1000);

  }



  render() {

    const styles = StyleSheet.create({
      input: {
        height: "80%",
        margin: 12,
        borderWidth: 1,
        padding: 10,
      },
    });

    return (
      
      <View>
          <Button
          title="Read from machine"
          onPress={() => {
            BluetoothService.readLastStrategy().then(v=>{
              if (v){
                v = JSON.stringify(JSON.parse(v), null, 2)
                // console.log(["var", v])
                this.setState({strategyStr: v })
              }
            }).catch(e=>{})
          }}
        />
        <TextInput
        style={styles.input}
        onChangeText={txt =>  this.setState({strategyStr: txt})}
        value={this.state.strategyStr}
        multiline={true}
      />
            <Text>{this.state.error}</Text>
        <Button
          title="Upload to machine"
          onPress={() => {
            try{
            var str = JSON.stringify(JSON.parse(this.state.strategyStr));
            } catch (e){
              this.setState({error: e.message})
              return;
            }
            
            BluetoothService.uploadStrategyRaw(str)


          }}
        />
      </View>
    );
  }
}