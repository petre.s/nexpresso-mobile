

import React, { Component } from 'react';


import { View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import BrewStrategyService from './brewStrategyService'

export default class BrewLimitStep extends Component {
    // componentDidUpdate() {

    uploadData() {
        if (this.state.pressureValid && this.state.stopTimeValid && this.state.yieldRateValid && this.state.yieldStopValid) {
            BrewStrategyService.updateBrewPhase({
                brewPhase: this.props.brewPhase,
                pressure: parseFloat(this.state.pressure),
                yieldRate: parseFloat(this.state.yieldRate),
                stopTime: parseInt(this.state.stopTime),
                yieldStop: parseInt(this.state.yieldStop),
            })
        }
    }

    constructor() {
        super()
        BrewStrategyService.subscribeToStrategy(phases => {
            phase = phases[this.props.brewPhase];

            this.setState({
                pressure: phase.pl.toString(),
                yieldRate: phase.yrl != null ? phase.yrl.toString() : "",
                yieldRateEnabled: (phase.yrl != null),
                yieldStop: phase.sy != null ? phase.sy.toString() : "",
                yieldStopEnabled: (phase.sy != null),
                stopTime: phase.st != null ? phase.st.toString() : "",
                stopTimeEnabled: (phase.st != null),
            })
        })

        this.state = {
            pressure: 2.2,
            pressureValid: true,
            yieldRateEnabled: false,
            yieldRate: null,
            yieldRateValid: true,
            yieldStopEnabled: false,
            yieldStop: null,
            yieldStopValid: true,
            stopTimeEnabled: false,
            stopTime: 10,
            stopTimeValid: true,
        }
    }

    render() {
        const styles = StyleSheet.create({
            input: {
                height: 30,
                width: 50,
                margin: 12,
                borderWidth: 1,
                padding: 10,
                borderRadius: 4
            },

            inputValid: {
                borderColor: "#c8c8c8"
            },

            inputInvalid: {
                borderColor: "red",
            },

            inputGroup: {
                flexDirection: "row",
                alignItems: "center",
                backgroundColor: "#f7f7f7",
                marginTop: 10,
                marginBottom: 10,
                paddingHorizontal: 10,
                borderRadius: 15,
                height: 50


            },
            macroGroup: {
                flexDirection: "column",
                // alignItems: "center",
                // backgroundColor: "#f7f7f7",
                // marginTop: 15,
                // marginBottom: 15,
                padding: 10,
                borderRadius: 15,
                // margin: 20,
                // height: 50,
                borderColor: "#f7f7f7",
                borderWidth: 2,
                borderStyle: "solid"
            },

            groupLabel: {
                fontSize: 17,
                width: 150
            },
            groupLabelInfo: {
                fontSize: 10
            },
            groupLabelInfoView: {
                height: 30,
                padding: 2,
                flexDirection: "column-reverse",
            },


            checkBox: {
                marginLeft: 10,
                height: 30
            },
            checkBoxView: {
                width: 30,
                marginRight: 10
            }

        });

        return (
            <View style={{
                // height: '100%',

                // justifyContent: 'center'
            }}>
                <View style={styles.macroGroup}>
                    <Text>Brew limits - {this.props.brewPhase} {this.state.pressure}</Text>

                    <View style={styles.inputGroup}>
                        <Text style={styles.groupLabel}>Pressure</Text>


                        <View style={styles.checkBoxView}>

                        </View>

                        <TextInput
                            style={[styles.input, this.state.pressureValid ? styles.inputValid : styles.inputInvalid]}
                            onChangeText={(val => {
                                let valid = parseFloat(val) && parseFloat(val) <= 9 && parseFloat(val) >= 2;
                                this.setState({ pressureValid: valid })
                                this.setState({ pressure: val })
                            })}
                            value={this.state.pressure}
                            onBlur={() => this.uploadData()}
                        />
                        <View style={styles.groupLabelInfoView}>
                            <Text style={styles.groupLabelInfo}>(bar)</Text>
                        </View>
                    </View>
                    <View style={styles.inputGroup}>
                        <Text style={styles.groupLabel}>Yield rate</Text>

                        <View style={styles.checkBoxView}>
                            <CheckBox
                                style={styles.checkBox}
                                animationDuration={0.05}
                                disabled={false}
                                boxType={"square"}
                                value={this.state.yieldRateEnabled}
                                onBlur={() => this.uploadData()}
                                onValueChange={(newValue) => {
                                    this.setState({ yieldRateEnabled: newValue, yieldRate: "" })
                                    setTimeout(() => this.uploadData(), 0)
                                }}
                            />
                        </View>
                        {this.state.yieldRateEnabled && <TextInput
                            style={[styles.input, this.state.yieldRateValid ? styles.inputValid : styles.inputInvalid]}
                            onChangeText={(val => {
                                let valid = parseFloat(val) && parseFloat(val) <= 10 && parseFloat(val) >= 0.1;
                                this.setState({ yieldRateValid: valid })

                                this.setState({ yieldRate: val })

                            })
                            }
                            value={this.state.yieldRate}
                            onBlur={() => this.uploadData()}
                        // keyboardType="numeric"
                        />}
                        {this.state.yieldRateEnabled && <View style={styles.groupLabelInfoView}>
                            <Text style={styles.groupLabelInfo}>(g/s)</Text>
                        </View>}
                    </View>
                </View>

                <View style={styles.macroGroup}>
                    <Text>Stop conditions</Text>
                    <View style={styles.inputGroup}>
                        <Text style={styles.groupLabel}>Time</Text>

                        <View style={styles.checkBoxView}>
                            <CheckBox
                                style={styles.checkBox}
                                animationDuration={0.05}
                                disabled={false}
                                boxType={"square"}
                                value={this.state.stopTimeEnabled}
                                onValueChange={(newValue) => {
                                    this.setState({ stopTimeEnabled: newValue, stopTime: "" })
                                    setTimeout(() => this.uploadData(), 0)
                                }}
                            />
                        </View>
                        {this.state.stopTimeEnabled && <TextInput
                            style={[styles.input, this.state.stopTimeValid ? styles.inputValid : styles.inputInvalid]}
                            onChangeText={(val => {
                                let valid = parseInt(val) && parseInt(val) <= 100 && parseInt(val) >= 1
                                this.setState({ stopTimeValid: valid })


                                this.setState({ stopTime: val })

                            })}
                            value={this.state.stopTime}
                            onBlur={() => this.uploadData()}
                        // placeholder="useless placeholder"
                        // keyboardType="numeric"

                        />}
                        {this.state.stopTimeEnabled && <View style={styles.groupLabelInfoView}>
                            <Text style={styles.groupLabelInfo}>(s)</Text>
                        </View>
                        }
                    </View>
                    <View style={styles.inputGroup}>
                        <Text style={styles.groupLabel}>Yield</Text>

                        <View style={styles.checkBoxView}>
                            <CheckBox
                                style={styles.checkBox}
                                animationDuration={0.05}
                                disabled={false}
                                boxType={"square"}
                                value={this.state.yieldStopEnabled}
                                onValueChange={(newValue) => {
                                    this.setState({ yieldStopEnabled: newValue, yieldStop: "" })
                                    setTimeout(() => this.uploadData(), 0)
                                }}

                            />
                        </View>

                        {this.state.yieldStopEnabled && <TextInput
                            style={[styles.input, this.state.yieldStopValid ? styles.inputValid : styles.inputInvalid]}
                            onChangeText={(val => {
                                let valid = parseInt(val) && parseInt(val) <= 100 && parseInt(val) >= 1
                                this.setState({ yieldStopValid: valid })


                                this.setState({ yieldStop: val })

                            })}
                            value={this.state.yieldStop}
                            onBlur={() => this.uploadData()}
                        // keyboardType="numeric"

                        />}
                        {this.state.yieldStopEnabled && <View style={styles.groupLabelInfoView}>
                            <Text style={styles.groupLabelInfo}>(g)</Text>
                        </View>}

                    </View>
                </View>
            </View>);
    }
}