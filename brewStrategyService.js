import {Subject } from 'rxjs';
import BluetoothService from './bluetoothService';

var phases = {
    0: {
        pl: 3,
        yrl: null,
        st: 10,
        sy: null
    },

    1: {
        pl: 6,
        yrl: null,
        st: null,
        sy: null
    },
};

var strategyObs = new Subject()
var strategyRawObs = new Subject()
BluetoothService.subscribeToStrategy(v=>BrewStrategyService.__onStrategyReceived(v))

const BrewStrategyService = {

    updateBrewPhase: function (phase) {
        phases[parseInt(phase.brewPhase)] = {
            pl: phase.pressure,
            yrl: phase.yieldRate,
            st: phase.stopTime,
            sy: phase.yieldStop
        }
        console.log(phases[phase.brewPhase])
        this.uploadStrategy();
    },

    subscribeToStrategy: function (value) {
        return strategyObs.subscribe(value)
    },

    __onStrategyReceived: function (value){
        strategyRawObs.next(value)
        console.log(["__onStrategyReceived", value])
        // let i = 0;
        // value.ph.forEach(phase => {
        //     phases[i] = phase;
        //     i++;
        // });
        // strategyObs.next(phases);
        // console.log(["received strategy", phases])
    },

    uploadStrategy: function () {
        // BluetoothService.uploadStrategy({ph:[phases[0], phases[1]]});
    },

    uploadStrategyRaw: function (rawStrategyStr) {
         BluetoothService.uploadStrategyRaw(rawStrategyStr);
    },

}

export default BrewStrategyService;