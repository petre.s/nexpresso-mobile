

import React, { Component } from 'react';



import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import SettingsScreen from './settingsScreen';
import TemperatureScreen from './temperatureScreen';
import BrewLimitStep from './brewLimitStep'
import DraggableFlatList, {
  ScaleDecorator
} from "react-native-draggable-flatlist";
import { DraxProvider, DraxList } from 'react-native-drax';

const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
const getBackgroundColor = (alphaIndex) => {
  switch (alphaIndex % 6) {
    case 0:
      return '#ffaaaa';
    case 1:
      return '#aaffaa';
    case 2:
      return '#aaaaff';
    case 3:
      return '#ffffaa';
    case 4:
      return '#ffaaff';
    case 5:
      return '#aaffff';
    default:
      return '#aaaaaa';
  }
}

const getHeight = (alphaIndex) => {
  let height = 50;
  if (alphaIndex % 2 === 0) {
    height += 10;
  }
  if (alphaIndex % 3 === 0) {
    height += 20;
  }
  return height;
}

const getItemStyleTweaks = (alphaItem) => {
  const alphaIndex = alphabet.indexOf(alphaItem);
  return {
    backgroundColor: getBackgroundColor(alphaIndex),
    height: getHeight(alphaIndex),
  };
};


export default class DragExample extends Component {

  constructor() {
    super()


    this.state = {
      alphaData: alphabet,
    };

  }



  render() {

    const styles = StyleSheet.create({
      container: {
        flex: 1,
        padding: 12,
        paddingTop: 40,
      },
      alphaItem: {
        backgroundColor: '#aaaaff',
        borderRadius: 8,
        margin: 4,
        padding: 4,
        justifyContent: 'center',
        alignItems: 'center',
      },
      alphaText: {
        fontSize: 28,
      },
    });

    return (
      <DraxProvider>
      <View style={styles.container}>
        <DraxList
          data={this.state.alphaData}
          renderItemContent={({ item }) => (
            <View style={[styles.alphaItem, getItemStyleTweaks(item)]}>
              <Text style={styles.alphaText}>{item}</Text>
            </View>
          )}
          onItemReorder={({ fromIndex, toIndex }) => {
            const newData = this.state.alphaData.slice();
            newData.splice(toIndex, 0, newData.splice(fromIndex, 1)[0]);
            this.setState({alphaData: newData})
          }}
          keyExtractor={(item) => item}
        />
      </View>
    </DraxProvider>
  );
    

  }

}