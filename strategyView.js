

import React, { Component } from 'react';



import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { SceneMap, TabBar, TabView } from 'react-native-tab-view';
import SettingsScreen from './settingsScreen';
import TemperatureScreen from './temperatureScreen';
import BrewLimitStep from './brewLimitStep'
import DraggableFlatList, {
  ScaleDecorator
} from "react-native-draggable-flatlist";
import { DraxProvider, DraxList } from 'react-native-drax';

const alphabet = [{ title: "Preinfusion", index: 0 }, { title: "Brew", index: 1 }]

export default class StrategyView extends Component {

  constructor() {
    super()


    this.state = {
      alphaData: alphabet,
      selectedStep: 0,
    };

  }

  render() {

    const styles = StyleSheet.create({
      screen: {
        flexDirection: "row",
        flex: 1,
        padding: 5
      },

      draggableList: {
        flex: 1,
        margin: 5,
        // height:800,

      },

      stepView: {
        flex: 5,
        margin: 2,
        marginRight: 7
      },


      container: {
        flex: 1,
        // padding: 12,
        paddingTop: 40,
        justifyContent: "center",

      },

      alphaItem: {
        // backgroundColor: '#aaaaff',
        borderRadius: 8,
        borderColor: "#f7f7f7",
        borderStyle: 'solid',
        borderWidth: 2,
        marginTop: 4,
        marginBottom: 4,
        padding: 4,
        // justifyContent: 'center',
        // alignItems: 'center',
      },
      newItemButton: {
        // backgroundColor: '#aaaaff',
        borderRadius: 8,
        borderColor: "#f7f7f7",
        borderStyle: 'dashed',
        borderWidth: 2,
        marginTop: 4,
        marginBottom: 4,
        padding: 4,
        justifyContent: 'center',
        alignItems: 'center',
        height: 40
      },
      alphaText: {
        fontSize: 12,
      },
    });

    var fields = [];
    for (let i = 0; i < 2; i++) {
      fields.push(<BrewLimitStep brewPhase={i}></BrewLimitStep>);
    }

    return (
      <View style={styles.screen}>
        <View style={styles.draggableList}>
          <DraxProvider>
            <View style={styles.container}>
              <DraxList
                data={this.state.alphaData}
                renderItemContent={({ item }) => (
                  <View style={styles.alphaItem} onTouchEnd={() => this.setState({ selectedStep: item.index })}>
                    <Text style={styles.alphaText}>{item.title}</Text>
                  </View>
                )}
                onItemReorder={({ fromIndex, toIndex }) => {
                  const newData = this.state.alphaData.slice();
                  newData.splice(toIndex, 0, newData.splice(fromIndex, 1)[0]);
                  this.setState({ alphaData: newData })
                }}
                keyExtractor={(item) => item}
              />
              <View style={styles.newItemButton} onTouchEnd={() => {
                const newData = this.state.alphaData.slice();
                newData.push("Step " + (newData.length + 1))
                this.setState({ alphaData: newData })
              }}>
                <Text style={styles.alphaText}>+</Text>
              </View>
            </View>
          </DraxProvider>
        </View>

        <View style={styles.stepView}>
        {fields[this.state.selectedStep]}
        {fields[1]}
        {/* <BrewLimitStep brewPhase={1}></BrewLimitStep> */}
          
        </View>
      </View>
    );


  }

}